package com.barcena;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("--Activity 1--");

        //appScanner
        Scanner appScanner = new Scanner(System.in);

        //Names
        String firstName;
        String lastName;


        System.out.println("Hi, what is your firstName?");
        firstName = appScanner.nextLine().trim();
        System.out.println("Hello, " + firstName + "! What is your lastName?");
        lastName = appScanner.nextLine().trim();
        System.out.println("Welcome, " + firstName + " " + lastName);
        String fullName = firstName + " " + lastName;

        //Scores

        double englishScore;
        double mathScore;
        double scienceScore;

        System.out.println("What is your score in English?");
        englishScore = appScanner.nextInt();
        System.out.println("What is your score in Mathematics?");
        mathScore = appScanner.nextInt();
        System.out.println("What is your score in Science?");
        scienceScore = appScanner.nextInt();

        double aveScore = (englishScore + mathScore + scienceScore) / 3;

        //finale

        System.out.println("Hi, "+ fullName +". Your average score is " + aveScore);
    }
}

